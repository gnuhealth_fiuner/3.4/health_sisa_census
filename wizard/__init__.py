# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from .batch_patient_census import *
from .party_census import *
from .patient_census import *
from .patient_create import *


