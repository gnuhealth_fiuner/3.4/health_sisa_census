# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from trytond.pool import Pool
from .party import *
from .health import *
from .wizard import *


def register():
    Pool.register(
        Party,
        PartyIdentifier,
        PartyAddress,
        PartyPatient,
        PatientData,
        Appointment,
        PatientCensusDataStart,
        CensusDataStart,
        PatientCreateStart,
        PatientCreateFound,
        #PatientCreateNotFound,
        #PatientCreateExistent,
        PatientCreateManual,
        PatientCreateData,
        SocialSecurity,
        BatchPatientCensusDataStart,
        BatchPatientCensusPrevalidation,
        BatchPatientCensusPrevalidationList,
        BatchPatientCensusValidation,
        BatchPatientCensusValidationList,
        module='health_sisa_census', type_='model')
    Pool.register(
        CensusData,        
        PatientCreate,
        PatientCensusData,
        BatchPatientCensusData,
        module='health_sisa_census', type_='wizard')
